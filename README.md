Orbital ReadMe
=======
##Project ReadMe
###Project Title: _HowToGo_
**Group name**: Pleiades

**Group members**:Huang Weiqi Victor and Chua Hou

####Table of contents
1. Overview
2. Ignition Links
3. Features

####Overview
Our project aims to revolutionise the direction giving process by providing an online web app that allows event planners to quickly create customised maps and directions for guests to easily find their way. Our targeted level of achievement is Project Gemini.

####Ignition Links
Our ignition slide:[SlideHere](http://puu.sh/i7TQv/c23bc5939d.png)

Ignition Video:[VideoHere](https://www.youtube.com/watch?v=QKuLLNVGvow)

####Features
#####Application Goals
* Able to create any QuikMap in under 5 minutes by reducing map complexity
* Able to be viewed easily on mobile for on-the-go viewing

#####Up to now
As of now, there are no major features implemented. Only the skeleton of our website has been set up.

#####Planned Features
* Core features (will definitely be implemented)
	* QuikMap Creation
		* QuikMaps contain a roughly drawn map and direction info that can be easily shared
	* Link-based sharing - no login, links will direct to specific pages
	* Viewable on desktop and mobile (Mobile interface will be made suitable for mobile)
	* Triangulation to allow users to find where they are relative to the map
* Secondary features (most likely implemented)
	* Downloadable QuikMaps for offline usage
	* A searchable database of QuikMaps
	* Google Location API integration, linking the QuikMap to a certain location
	* Facebook, Twitter and Google integration, for the sharing of the created QuikMaps
	* Link shortening using bit.ly or alternatives
* Tertiary features (to be KIVed)
	* Account creation, mainly for those who creates many QuikMaps and wants to keep track of them
	* A way to make QuikMaps on the go, on the phones